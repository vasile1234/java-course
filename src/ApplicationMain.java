import java.util.Scanner;

public class ApplicationMain {
	
	public static void main(String[] args) {
		
		
		Car golfiv = new Car();
		
		golfiv.brand = "Volkswagen";
		golfiv.model = "Golf IV";
		golfiv.cityConsumption = 11;
		golfiv.highwayConsumption = 7;
		golfiv.secondHand = true;
		golfiv.firstLetter = 'G';
		
		
		golfiv.describe ();
	}
}
