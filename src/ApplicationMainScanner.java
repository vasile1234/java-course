import java.util.Scanner;

public class ApplicationMainScanner {
	
	public static void main(String[] args) {
		//create the object for reading from console
		Scanner in = new Scanner(System.in);
		
		System.out.println("Enter a numeric value: ");
		
		int a = (int) in.nextDouble();    //read an int value
		System.out.print("You typed: ");
		System.out.print(a);
	} // end of main
} // end of class
