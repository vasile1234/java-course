import java.util.Scanner;

public class Car {
		
		String brand;
		String model;
		Integer year;
		Integer cityConsumption;
		Integer highwayConsumption;
		boolean secondHand;
		char firstLetter;
		
		Scanner in = new Scanner(System.in);
		int a = in.nextInt();
		
		
		void describe() {
			
			System.out.println("The car " + model + " from group " + brand + " fabricated in " + year + " has city consumption " + cityConsumption  + " and highway consumption " + highwayConsumption + "\n");
			System.out.println("Is the car second hand? " + secondHand + "\n");
			System.out.println("The first letter from the car name is: " + firstLetter + "\n");
			System.out.println("The average consumption is: " + (cityConsumption + highwayConsumption)/2);
			System.out.println(a);
			
		}
}
