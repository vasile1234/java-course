import java.util.Scanner;

public class Game {

	public static void main(String[] args) {

		for (int i = 0; i < 3; i++) {
			Scanner in = new Scanner(System.in);

			System.out.println("John, tell me your number: ");
			int a = in.nextInt();

			System.out.println("Bob, tell me your name: ");
			int b = in.nextInt();

			if (a > b) {
				System.out.println("Jone have won!");
			} else if (a < b) {
				System.out.println("Bob have won!");
			} else {
				System.out.println("It's a tie!");
			}
		}
	}
}
